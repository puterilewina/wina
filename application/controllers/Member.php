<?php defined('BASEPATH') or exit('No direct script access allowed');
class Member extends CI_Controller
{
	public $data = [];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(['ion_auth', 'form_validation']);
		$this->load->helper(['url', 'language']);
		$this->load->model('M_auth');
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->lang->load('auth');
		if (!$this->ion_auth->logged_in()) {
			redirect('auth/login', 'refresh');
		}
		if (!$this->ion_auth->in_group('members')) {
			redirect('/');
		}
		$this->user = $this->ion_auth->user()->row();
		$this->group = $this->ion_auth->get_users_groups($this->user->id)->result();
		$this->load->model('M_Model');
	}

	public function index()
	{
		$datas['user'] = $this->user;
		$datas['group'] = $this->group[0]->name;
		$datas['sampah'] = $this->M_Model->get('sampah');
		$datas['id_user'] = $this->M_Model->get_by('users', ['id' => $this->user->id]);
		$datas['history'] = $this->M_Model->get_like('history', ['id_user' => $this->user->id]);
		$datas['total'] = 0;
		foreach ($datas['history']->result() as $key => $value) {
			$datas['total'] += $value->total;
		}
		$this->load->view('dashboard/member/dashboard', $datas);
	}
	function history_dummy()
    {
        $this->form_validation->set_rules('jenis', 'jenis', 'required');
        $this->form_validation->set_rules('berat', 'berat', 'required|decimal');
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $this->session->set_flashdata('errors', $errors);
            $this->session->set_flashdata('input', $this->input->post());
            echo '<script>alert("Data Tidak Lengkap");window.location.href="' . base_url('Member') . '";</script>';
        } else {

            $jenis = $this->M_Model->get_by('sampah', ["id" => $this->input->post('jenis')]);
            $data = [
                'id_user' => $this->input->post('id'),
                'id_transaksi' => time(),
                'sampah' => $jenis->jenis,
                'berat' => $this->input->post('berat'),
                'total' => $jenis->harga * $this->input->post('berat'),
            ];

            $this->M_Model->tambah("history", $data);
            
            $datas['history'] = $this->M_Model->get_like('history', ['id_user' => $this->input->post('id')]);
            $datas['total'] = 0;
            foreach ($datas['history']->result() as $key) {
                $datas['total'] += $key->total;
            }
            $this->M_Model->edit("users", ["id" => $this->input->post('id')], ["saldo" => $datas['total']]);
            echo '<script>alert("Data Dummy Berhasil di tambah");window.location.href="' . base_url('Member') . '";</script>';
        }
    }
}
