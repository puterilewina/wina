<?php
class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(['ion_auth', 'form_validation']);
        $this->load->helper(['url', 'language']);
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->lang->load('auth');
        if (!$this->ion_auth->is_admin()) {
            redirect('/');
        }
        $this->load->model('M_Auth');
        $this->load->model('M_Model');
        $this->user = $this->ion_auth->user()->row();
    }
    function index()
    {
        $datas['list_user'] = $this->M_Model->join();
        $datas['user'] = $this->user;
        $this->load->view('dashboard/admin/dashboard', $datas);
    }
    function history($id)
    {
        $datas['user'] = $this->user;
        $datas['sampah'] = $this->M_Model->get('sampah');
        $datas['id_user'] = $this->M_Model->get_by('users', ['id' => $id]);
        $datas['history'] = $this->M_Model->get_like('history', ['id_user' => $id]);
        $datas['total'] = 0;
        foreach ($datas['history']->result() as $key) {
            $datas['total'] += $key->total;
        }
        $this->load->view('dashboard/admin/history', $datas);
    }
    function history_dummy()
    {
        $this->form_validation->set_rules('jenis', 'jenis', 'required');
        $this->form_validation->set_rules('berat', 'berat', 'required|decimal');
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $this->session->set_flashdata('errors', $errors);
            $this->session->set_flashdata('input', $this->input->post());
            echo '<script>alert("Data Tidak Lengkap");window.location.href="' . base_url('Admin/history/' . $this->input->post('id')) . '";</script>';
        } else {

            $jenis = $this->M_Model->get_by('sampah', ["id" => $this->input->post('jenis')]);
            $data = [
                'id_user' => $this->input->post('id'),
                'id_transaksi' => time(),
                'sampah' => $jenis->jenis,
                'berat' => $this->input->post('berat'),
                'total' => $jenis->harga * $this->input->post('berat'),
            ];

            $this->M_Model->tambah("history", $data);
            
            $datas['history'] = $this->M_Model->get_like('history', ['id_user' => $this->input->post('id')]);
            $datas['total'] = 0;
            foreach ($datas['history']->result() as $key) {
                $datas['total'] += $key->total;
            }
            $this->M_Model->edit("users", ["id" => $this->input->post('id')], ["saldo" => $datas['total']]);
            echo '<script>alert("Data Dummy Berhasil di tambah");window.location.href="' . base_url('Admin/history/' . $this->input->post('id')) . '";</script>';
        }
    }
    function sampah()
    {
        $datas['user'] = $this->user;
        $datas['data'] = $this->M_Model->get('sampah');
        $this->load->view('dashboard/admin/sampah', $datas);
    }
    function sampah_tambah()
    {
        $this->form_validation->set_rules('jenis', 'jenis', 'trim|required|is_unique[sampah.jenis]');
        $this->form_validation->set_rules('harga', 'harga', 'required');
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $this->session->set_flashdata('errors', $errors);
            $this->session->set_flashdata('input', $this->input->post());
            echo '<script>alert("Jenis sampah sudah terdaftar");window.location.href="' . base_url('Admin/sampah') . '";</script>';
        } else {
            $data = [
                'jenis' => $this->input->post('jenis'),
                'harga' => $this->input->post('harga'),
            ];
            $this->M_Model->tambah("sampah", $data);
            echo '<script>alert("Jenis sampah berhasi di tambah");window.location.href="' . base_url('Admin/sampah') . '";</script>';
        }
    }
    function sampah_edit()
    {
        $this->form_validation->set_rules('jenis', 'jenis', 'required');
        $this->form_validation->set_rules('harga', 'harga', 'required');
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $this->session->set_flashdata('errors', $errors);
            $this->session->set_flashdata('input', $this->input->post());
            echo '<script>alert("Jenis sampah sudah terdaftar");window.location.href="' . base_url('Admin/sampah') . '";</script>';
        } else {
            $data = [
                'jenis' => $this->input->post('jenis'),
                'harga' => $this->input->post('harga'),
            ];
            $where = [
                'id' => $this->input->post('id'),
            ];
            $this->M_Model->edit("sampah", $where, $data);
            echo '<script>alert("Jenis sampah berhasi di ubah");window.location.href="' . base_url('Admin/sampah') . '";</script>';
        }
    }
    function sampah_hapus()
    {
        $this->M_Model->delete("sampah", ['id' => $this->input->post('id')]);
        echo '<script>alert("Jenis sampah berhasi di hapus");window.location.href="' . base_url('Admin/sampah') . '";</script>';
    }
    function TambahPengepul()
    {
        $this->form_validation->set_rules('identity', 'identity', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('username', 'username', 'required|min_length[5]|max_length[30]|is_unique[users.username]');
        $this->form_validation->set_rules('password', 'password', 'required|trim');
        if ($this->form_validation->run() == FALSE) {
            $errors = $this->form_validation->error_array();
            $this->session->set_flashdata('errors', $errors);
            $this->session->set_flashdata('input', $this->input->post());
            echo '<script>alert("Email atau Username sudah terdaftar");window.location.href="' . base_url('Auth/register') . '";</script>';
        } else {
            $nama       = $this->input->post('username');
            $email     = $this->input->post('identity');
            $password    = $this->input->post('password');
            $pass = password_hash($password, PASSWORD_DEFAULT);
            $data = [
                'email' => $email,
                'username' => $nama,
                'password' => $pass,
                'created_on' => time(),
                'active' => 1
            ];
            $this->M_Auth->tambah("users", $data);
            $e = $this->M_Auth->aktivasi($email);
            $insert = $this->M_Auth->aktifasi($e->id, 3);
            if ($insert) {
                echo '<script>alert("Anda berhasil melakukan register pengepul. Silahkan cek email anda untuk aktifasi akun");window.location.href="' . base_url('Admin') . '";</script>';
            }
            echo '<script>alert("Gagal Melakukan Registrasi!");window.location.href="' . base_url('Admin') . '";</script>';
        }
    }
    function CekPemesanan_post()
    {
    }
}
