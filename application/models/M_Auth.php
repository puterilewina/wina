<?php
class M_Auth extends CI_Model
{
  public function tambah($table, $data)
  {
    return $this->db->insert($table, $data);
  }
  public function GetWhere($table, $where)
  {
    $this->db->select('*');
    $this->db->from($table);
    $this->db->where($where);
    $query = $this->db->get();
    return $query->row_array();
  }
  public function aktifasi($data,$id)
  {
      $datas = array('group_id' => $id, 'user_id' => $data);
      return $this->db->insert('users_groups', $datas);
  }
  public function aktivasi($key)
  {
      $this->db->select('id')->from('users')->where('email', $key);
      return $this->db->get()->row();
  }
}
