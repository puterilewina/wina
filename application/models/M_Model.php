<?php
class M_Model extends CI_Model
{
  public function tambah($table, $data)
  {
    return $this->db->insert($table, $data);
  }
  public function edit($table,$where, $data)
  {
    $this->db->where($where);
    $this->db->update($table, $data);
  }
  public function delete($table,$where)
  {
    $this->db->where($where);
    $this->db->delete($table);
  }
  public function get_like($table, $data)
  {
    $this->db->select('*');
    $this->db->from($table);
    $this->db->where($data);
    $query = $this->db->get();
    return $query;
  }
  public function get_by($table, $data)
  {
    $this->db->select('*');
    $this->db->from($table);
    $this->db->where($data);
    $query = $this->db->get();
    return $query->row();
  }
	public function getlast_by($table, $data)
  {
    $this->db->select('*');
    $this->db->from($table);
    $this->db->where($data);
		$this->db->order_by('id', 'DESC');
		$this->db->limit(1);  
    $query = $this->db->get();
    return $query->row();
  }
  public function get($table)
  {
    $this->db->select('*');
    $this->db->from($table);
    return $this->db->get();
  }
  public function Join()
  {
    $this->db->select('*');
    $this->db->from('users_groups');
    $this->db->join('users', 'users_groups.user_id = users.id','full outer');
    // $this->db->join('history', 'users_groups.user_id = history.id_user','full outer');
    $query = $this->db->get();
    return $query;
  }
}
