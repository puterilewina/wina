<?php include("_header.php"); ?>
<?php include("_navbar.php"); ?>
<div class="container-fluid">
	<div class="page-header min-height-300 border-radius-xl mt-4" style="background-image: url('<?= base_url() ?>assets/img/curved-images/curved0.jpg'); background-position-y: 50%;">
		<span class="mask bg-gradient-primary opacity-6"></span>
	</div>
	<div class="card card-body blur shadow-blur mx-4 mt-n6 overflow-hidden">
		<div class="row gx-4">
			<div class="col-auto">
				<div class="avatar avatar-xl position-relative">
					<img src="<?= base_url() ?>assets/img/bruce-mars.jpg" alt="profile_image" class="w-100 border-radius-lg shadow-sm">
				</div>
			</div>
			<div class="col-auto my-auto">
				<div class="h-100">
					<h5 class="mb-1">
						<?= $user->username; ?>
					</h5>
					<p class="mb-0 font-weight-bold text-sm">
						<?= $group; ?>
					</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto mt-3">
				<div class="nav-wrapper position-relative end-0">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid py-4">
  <div class="row my-4">
    <div class="col-lg-12 col-md-6 mb-md-0 mb-4">
      <div class="card">
        <div class="card-header pb-0">
          <div class="row">
            <div class="col-lg-6 col-7">
              <h6>History Transaksi <?php echo $id_user->username ?></h6>
            </div>
            <div class="col-lg-6 col-5 my-auto">
              <div class="dropdown float-lg-end pe-4">
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal"><i class="fas fa-plus"></i>
                  Tambah History Dummy
                </button>
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah History Dummy</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <form role="form" action="<?php echo base_url() . 'Member/history_dummy' ?>" method="post">
                        <div class="modal-body">
                          <label>Jenis</label>
                          <div class="mb-3">
                            <select name="jenis" class="form-select" aria-label="Default select example">
                              <?php foreach ($sampah->result() as $key) : ?>
                                <option value="<?php echo $key->id ?>"><?php echo $key->jenis ?></option>
                              <?php endforeach; ?>
                            </select>
                          </div>
                          <label>Berat</label>
                          <div class="mb-3">
                            <input type="number" name="berat" class="form-control" step="0.01">
                          </div>
                          <input type="hidden" name="id" value="<?php echo $id_user->id ?>">
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card-body px-2 pb-2">
          <div class="table-responsive">
            <table class="table align-items-center mb-0 table-striped" id="table1">
              <thead>
                <tr>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">no</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">ID-Transaksi</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">jenis sampah</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">berat</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">waktu</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">saldo</th>

                </tr>
              </thead>
              <tbody>
                <?php $no = 1 ?>
                <?php foreach ($history->result() as $key) : ?>
                  <tr>
                    <td class="align-middle text-sm">
                      <span class="text-xs font-weight-bold"><?php echo $no ?></span>
                    </td>
                    <td class="align-middle text-sm">
                      <span class="text-xs font-weight-bold"><?php echo $key->id_transaksi ?></span>
                    </td>
                    <td class="align-middle text-sm">
                      <span class="text-xs font-weight-bold"><?php echo $key->sampah ?></span>
                    </td>
                    <td class="align-middle text-sm">
                      <span class="text-xs font-weight-bold"><?php echo $key->berat ?> Kg</span>
                    </td>
                    <td class="align-middle text-sm">
                      <span class="text-xs font-weight-bold"><?php echo $key->time ?></span>
                    </td>
                    <td class="align-middle text-sm">
                      <span class="text-xs font-weight-bold">Rp.<?php echo $key->total ?></span>
                    </td>
                  </tr>
                  <?php $no += 1 ?>
                <?php endforeach; ?>
              </tbody>
              <tfoot>
                <tr>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Total</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2"></th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2"></th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2"></th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2"></th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder ps-2">Rp.<?php echo $total ?></th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row my-4">
    <div class="col-lg-9">
      <div class="card z-index-2">
        <div class="card-header pb-0">
          <h6>Budget History</h6>
          <p class="text-sm">
            <span class="font-weight-bold">10 Data Terakhit</span>
          </p>
        </div>
        <div class="card-body p-3">
          <div class="chart">
            <canvas id="chart-line" class="chart-canvas" height="300"></canvas>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="card z-index-2">
        <div class="card-header pb-0">
          <h6>Sales overview</h6>
          <p class="text-sm">
            <i class="fa fa-arrow-up text-success"></i>
            <span class="font-weight-bold">4% more</span> in 2021
          </p>
        </div>
        <div class="card-body p-3">
          <div class="chart">
            <!-- <canvas id="chart-line" class="chart-canvas" height="300"></canvas> -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include("_footer.php"); ?>
</div>
<script src="<?= base_url() ?>assets/js/plugins/chartjs.min.js"></script>
<script>
  var canvas = document.getElementById("chart-line");
  const ctx2 = canvas.getContext('2d');

  var gradientStroke1 = ctx2.createLinearGradient(0, 230, 0, 50);

  gradientStroke1.addColorStop(1, 'rgba(203,12,159,0.2)');
  gradientStroke1.addColorStop(0.2, 'rgba(72,72,176,0.0)');
  gradientStroke1.addColorStop(0, 'rgba(203,12,159,0)');
  new Chart(ctx2, {
    type: "line",
    data: {
      labels: [
        <?php $do = 0; ?>
        <?php foreach ($history->result() as $key) : ?>
          <?php $do += 1; ?>
          '<?php echo $key->time; ?>',
          <?php if ($do >= 10) : ?>
            <?php break; ?>
          <?php endif; ?>
        <?php endforeach; ?>
      ],
      datasets: [{
        label: "Money Budget",
        tension: 0.4,
        borderWidth: 0,
        pointRadius: 0,
        borderColor: "#cb0c9f",
        borderWidth: 3,
        backgroundColor: gradientStroke1,
        fill: true,

        data: [
          <?php $datasi = 0; ?>
          <?php foreach ($history->result() as $key) : ?>
            <?php $datasi += $key->total; ?>
            <?php echo $datasi; ?>,
          <?php endforeach; ?>
        ],
        maxBarThickness: 6

      }, ],
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        legend: {
          display: false,
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      },
      scales: {
        y: {
          grid: {
            drawBorder: false,
            display: true,
            drawOnChartArea: true,
            drawTicks: false,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            padding: 10,
            color: '#b2b9bf',
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
        x: {
          grid: {
            drawBorder: false,
            display: false,
            drawOnChartArea: false,
            drawTicks: false,
            borderDash: [5, 5]
          },
          ticks: {
            display: true,
            color: '#b2b9bf',
            padding: 20,
            font: {
              size: 11,
              family: "Open Sans",
              style: 'normal',
              lineHeight: 2
            },
          }
        },
      },
    },
  });
</script>
<?php include("_script.php"); ?>
