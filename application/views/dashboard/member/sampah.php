<?php include("_header.php"); ?>
<?php include("_navbar.php"); ?>
<div class="container-fluid py-4">
  <div class="row my-4">
    <div class="col-lg-12 col-md-6 mb-md-0 mb-4">
      <div class="card">
        <div class="card-header pb-0">
          <div class="row">
            <div class="col-lg-6 col-7">
              <h6>Jenis Sampah</h6>
            </div>
            <div class="col-lg-6 col-5 my-auto">
              <div class="dropdown float-lg-end pe-4">
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal"><i class="fas fa-plus"></i>
                Tambah Jenis Sampah
                </button>
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Jenis Sampah</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <form role="form" action="<?php echo base_url() . 'Admin/sampah_tambah' ?>" method="post">
                        <div class="modal-body">
                          <label>Jenis</label>
                          <div class="mb-3">
                            <input type="text" name="jenis" class="form-control">
                          </div>
                          <label>Harga/Kg</label>
                          <div class="mb-3">
                            <input type="number" name="harga" class="form-control">
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card-body px-2 pb-2">
          <div class="table-responsive">
            <table class="table align-items-center mb-0 table-striped" id="table1">
              <thead>
                <tr>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">no</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">jenis sampah</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Harga</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1 ?>
                <?php foreach ($data->result() as $key) : ?>
                  <tr>
                    <td class="align-middle text-sm">
                      <span class="text-xs font-weight-bold"><?php echo $no ?></span>
                    </td>
                    <td class="align-middle text-sm">
                      <span class="text-xs font-weight-bold"><?php echo $key->jenis ?></span>
                    </td>
                    <td class="align-middle text-sm">
                      <span class="text-xs font-weight-bold"><?php echo $key->harga ?> Kg</span>
                    </td>
                    <td class="align-middle text-sm">
                      <span class="badge badge-sm bg-warning" data-bs-toggle="modal" data-bs-target="#edit<?php echo $key->id ?>"><i class="fas fa-edit"></i></span>
                      <span class="badge badge-sm bg-danger" data-bs-toggle="modal" data-bs-target="#hapus<?php echo $key->id ?>"><i class="fa-solid fa-xmark"></i></i></span>
                    </td>
                  </tr>
                  <div class="modal fade" id="edit<?php echo $key->id ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Edit Jenis Sampah</h5>
                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form role="form" action="<?php echo base_url() . 'Admin/sampah_edit'?>" method="post">
                          <div class="modal-body">
                            <label>Jenis</label>
                            <div class="mb-3">
                              <input type="text" name="jenis" class="form-control" value="<?php echo $key->jenis ?>">
                            </div>
                            <label>Harga/Kg</label>
                            <div class="mb-3">
                              <input type="text" name="harga" class="form-control" value="<?php echo $key->harga ?>">
                            </div>
                            <input type="hidden" name="id" class="form-control" value="<?php echo $key->id ?>">
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div class="modal fade" id="hapus<?php echo $key->id ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Hapus Jenis sampah</h5>
                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form role="form" action="<?php echo base_url() . 'Admin/sampah_hapus'?>" method="post">
                          <div class="modal-body">
                            <h5 class="modal-title text-center">Yakin ingin menghapus?</h5>
                            <input type="hidden" name="id" class="form-control" value="<?php echo $key->id ?>">
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <?php $no += 1 ?>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include("_footer.php"); ?>
</div>
<?php include("_script.php"); ?>