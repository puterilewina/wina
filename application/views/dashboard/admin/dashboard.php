<?php include("_header.php"); ?>
<?php include("_navbar.php"); ?>
<div class="container-fluid py-4">
  <div class="row my-4">
    <div class="col-lg-12 col-md-6 mb-md-0 mb-4">
      <div class="card">
        <div class="card-header pb-0">
          <div class="row">
            <div class="col-lg-6 col-7">
              <h6>List User</h6>
            </div>
            <div class="col-lg-6 col-5 my-auto">
              <div class="dropdown float-lg-end pe-4">
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal"><i class="fas fa-plus"></i>
                  Tambah Pengepul
                </button>
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Pengepul</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <form role="form" action="<?php echo base_url() . 'Admin/TambahPengepul' ?>" method="post">
                        <div class="modal-body">
                          <label>Username</label>
                          <div class="mb-3">
                            <input type="text" name="username" class="form-control" placeholder="name" aria-label="name" aria-describedby="name-addon">
                          </div>
                          <label>Email</label>
                          <div class="mb-3">
                            <input type="email" name="identity" class="form-control" placeholder="Email" aria-label="Email" aria-describedby="email-addon">
                          </div>
                          <label>Password</label>
                          <div class="mb-3">
                            <input type="password" name="password" class="form-control" placeholder="Password" aria-label="Password" aria-describedby="password-addon">
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card-body px-2 pb-2">
          <div class="table-responsive">
            <table class="table align-items-center mb-0 table-striped" id="table1">
              <thead>
                <tr>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">no</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Nama</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">email</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">saldo</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">role</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">history</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1 ?>
                <?php foreach ($list_user->result() as $key) : ?>
                  <tr>
                    <td class="align-middle text-sm">
                      <span class="text-xs font-weight-bold"><?php echo $no ?></span>
                    </td>
                    <td class="align-middle text-sm">
                      <span class="text-xs font-weight-bold"><?php echo $key->username ?></span>
                    </td>
                    <td class="align-middle text-sm">
                      <span class="text-xs font-weight-bold"><?php echo $key->email ?></span>
                    </td>
                    <td class="align-middle text-sm">
                      <span class="text-xs font-weight-bold">Rp.<?php echo $key->saldo ?></span>
                    </td>
                    <td class="align-middle text-sm">
                      <?php if ($key->group_id === '1') : ?>
                        <span class="badge badge-sm bg-gradient-success">Administrator</span>
                      <?php elseif ($key->group_id === '2') : ?>
                        <span class="badge badge-sm bg-gradient-secondary">Member</span>
                      <?php else : ?>
                        <span class="badge badge-sm bg-gradient-info">Pengepul</span>
                      <?php endif ?>
                    </td>
                    <td class="align-middle text-sm">
                      <?php if ($key->group_id == '1') : ?>
                        <!-- <span class="badge badge-sm bg-gradient-success">Administrator</span> -->
                      <?php elseif ($key->group_id == "2") : ?>
                        <a href="<?php echo base_url() . 'Admin/history/' . $key->id ?>" class="badge badge-sm bg-gradient-primary"><i class="fa-solid fa-eye"></i></a>
                      <?php else : ?>
                        <!-- <span class="badge badge-sm bg-gradient-info">Pengepul</span> -->
                      <?php endif ?>
                    </td>
                  </tr>
                  <?php $no += 1 ?>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include("_footer.php"); ?>
</div>
<?php include("_script.php"); ?>